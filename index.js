var extname = require('path').extname;
var Metalsmith = require('metalsmith');
var sass = require('metalsmith-sass');

/**
 * Build.
 */

var metalsmith = Metalsmith(__dirname)
	.source('src')
	.destination('build')
	.use(sass({
		file: './src/*.scss',
		//outputStyle: "compressed"
	}))
	.build(function(err){
		if (err) throw err;
	});
